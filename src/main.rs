#[macro_use]
extern crate serde_derive;
extern crate serde_xml_rs;

use serde::de::{Deserialize, Deserializer};
use serde_xml_rs::from_str;

const INPUT: &str = r#"<Book>
<Title>blah blah</Title>
<Authors>
  <Author>
    <FullName>john doe</FullName>
    <Email>john@thedoes.com</Email>
  </Author>
  <Author>
    <FullName>jane doe</FullName>
    <Email>jane@thedoes.com</Email>
  </Author>
</Authors></Book>"#;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Book {
    pub title: String,
    #[serde(deserialize_with = "author_from_authors")]
    pub authors: Vec<Author>,
}

fn author_from_authors<'de, D>(d: D) -> Result<Vec<Author>, D::Error>
where
    D: Deserializer<'de>,
{
    let authors = Authors::deserialize(d).unwrap();
    Ok(authors.author)
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Authors {
    pub author: Vec<Author>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Author {
    #[serde(rename = "FullName")]
    name: String,
    #[serde(rename = "Email")]
    email: String,
}

fn main() {
    let book: Book = from_str(INPUT).unwrap();
    println!("{:?}", book);
    for a in book.authors {
        println!("{:?}",a)
    }
}
